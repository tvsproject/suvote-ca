#!/bin/bash
read -s -p "Enter SubCA Password: " subCAPassword
echo "About to Suspend $1"
openssl ca -config ca.conf -passin pass:$subCAPassword -revoke $1 -crl_reason certificateHold
if [ "$?" -ne "0" ]; then
	echo "Suspend failed"
else
	echo "Suspended $1"
	echo "You must now create a new CRL!"
fi



