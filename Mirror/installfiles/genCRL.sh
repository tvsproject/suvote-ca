#!/bin/bash
SubCAName="${PWD##*/}"
read -s -p "Enter SubCA Password: " subCAPassword
echo "About to generate CRL"
cp  --backup=t "./$SubCAName.crl" "./crldb/"
openssl ca -batch -config ca.conf -passin pass:$subCAPassword -gencrl -out "./$SubCAName.crl"
if [ "$?" -ne "0" ]; then
	echo "CRL Generation failed"
else
	echo "CRL Generated"
fi



