#!/bin/bash
CA_DIR="VVoteRootCA"
CERTS_DIR="certsdb"
CRL_DIR="crldb"
CERT_REQS="certreqs"
PRIVATE_DIR="private"
ISSUED_DIR="issued"
PROCESSED_CSRS="processed"

read -s -p "Enter SubCA Password: " subCAPassword
shopt -s nullglob
for f in ./$CERT_REQS/*.csr


do
	echo "Signing $f"
  filename=${f##*/}
	##Strip the CertReq from the filename
  filename=${filename%CertReq*}
	if [ -f "./serial" ];
	then
		##Serial file exists use standard method
  	openssl ca -notext -batch -passin pass:$subCAPassword -out "./$ISSUED_DIR/${filename%.*}.cert" -policy policy_anything -config ca.conf -infiles "$f"
	else
    ##First run, need to init serial file
  	echo "Serial File does not exist, will initialise."
		openssl ca -create_serial -notext -batch -passin pass:$subCAPassword -out "./$ISSUED_DIR/${filename%.*}.cert" -policy policy_anything -notext -config ca.conf -infiles "$f"
	fi
  if [ "$?" -ne "0" ]; then
    echo "CSR Signing failed, will leave CSR in $CERT_REQS"
  else
  	mv $f "./$PROCESSED_CSRS/"
	  echo "Moved $f to $PROCESSED_CSRS"
		echo "Certificate is in ./$ISSUED_DIR/${filename%.*}.cert"
  fi
	##openssl ca -config ca.conf -infiles "./$CERT_REQS/$1"
done

