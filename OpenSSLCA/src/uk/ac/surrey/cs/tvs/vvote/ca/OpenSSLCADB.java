/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.ca;

/**
 * Enum that provides access to the different fields in the tab seperate index.txt generated by the OpenSSL CA.
 * 
 * @author Chris Culnane
 * 
 */
public enum OpenSSLCADB {
  STATE, VALID_UNTIL, REVOKE_DATE_REASON, SERIAL_NO, UKNOWN, COMMON_NAME

}
