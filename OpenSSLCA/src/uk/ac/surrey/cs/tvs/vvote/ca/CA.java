/*******************************************************************************
 * Copyright (c) 2014 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vvote.ca;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Reads the OpenSSL index file and provides access to the entries. It currently provides two CA functionalities and one utility
 * function. The two CA functionalities are:
 * <ol>
 * <li>Listing current certificates</li>
 * <li>Re-instating an onhold Certificate</li>
 * </ol>
 * The additional utility function is to read and show a Certificate
 * 
 * @author Chris Culnane
 * 
 */
public class CA {

  /**
   * Constant holding the name of the default index file
   */
  private static final String           DEFAULT_INDEX = "index.txt";

  /**
   * Index file to read and save to
   */
  private String                        indexFile;

  /**
   * ArrayList of all entries in the index
   */
  private ArrayList<CAIndexEntry>       caEntries     = new ArrayList<CAIndexEntry>();

  /**
   * Entries indexed by the serial number of the certificates
   */
  private HashMap<String, CAIndexEntry> serialNoMap   = new HashMap<String, CAIndexEntry>();

  /**
   * Loads CA using DEFAULT_INDEX file
   * 
   * @throws IOException
   * @throws CAEntryException
   */
  public CA() throws IOException, CAEntryException {
    this(DEFAULT_INDEX);
  }

  /**
   * Loads CA using the specified index file. If the index file does not exist an IOException will be thrown.
   * 
   * @throws IOException
   * @throws CAEntryException
   * 
   */
  public CA(String indexFile) throws IOException, CAEntryException {
    this.indexFile = indexFile;
    File f = new File(indexFile);
    if (f.exists()) {
      BufferedReader br = null;
      try {
        br = new BufferedReader(new FileReader(f));
        String line;
        while ((line = br.readLine()) != null) {
          CAIndexEntry entry = new CAIndexEntry(line);
          caEntries.add(entry);
          serialNoMap.put(entry.getField(OpenSSLCADB.SERIAL_NO), entry);
        }
      }
      finally {
        if (br != null) {
          br.close();
        }
      }

    }
    else {
      throw new IOException("CA Index file does not exist");
    }
  }

  /**
   * Saves the modified CA index file back to the original location
   * 
   * It looks throw the entries outputting their values
   * 
   * @throws IOException
   */
  public void save() throws IOException {

    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(this.indexFile));
      for (CAIndexEntry entry : caEntries) {
        bw.write(entry.getValue());
        bw.write("\n");
      }
    }
    finally {
      if (bw != null) {
        bw.close();
      }
    }
  }

  /**
   * Creates a string containing one entry per line
   * 
   * @return String with each entry on a new line
   */
  public String list() {
    StringBuffer sb = new StringBuffer();
    for (CAIndexEntry entry : caEntries) {
      sb.append(entry.toString());
      sb.append("\n");
    }
    return sb.toString();
  }

  /**
   * Searches for the serial number specified and if it exists reinstates that certificate entry. If the certificate entry cannot be
   * found it throws a ModificationException
   * 
   * @param serialString
   *          String serial number of certificate to reinstate
   * @throws ModificationException
   */
  public void reinstate(String serialString) throws ModificationException {

    if (this.serialNoMap.containsKey(serialString)) {
      CAIndexEntry entry = this.serialNoMap.get(serialString);
      entry.reinstate();
    }
    else {
      throw new ModificationException("Could not find certificate");
    }
  }

  /**
   * Searches for the serial number specified in the certificate, and if it exists reinstates that certificate entry. If the
   * certificate entry cannot be found it throws a ModificationException
   * 
   * @param cert
   *          X509Certificate to reinstate
   * @throws ModificationException
   */
  public void reinstate(X509Certificate cert) throws ModificationException {
    String serialString = cert.getSerialNumber().toString(16).toUpperCase();
    if (this.serialNoMap.containsKey(serialString)) {
      CAIndexEntry entry = this.serialNoMap.get(serialString);
      entry.reinstate();
    }
    else {
      throw new ModificationException("Could not find certificate");
    }
  }

  /**
   * Utility method to learn an X509Certificate from a specified file
   * 
   * @param certFile
   *          String containing path of the ceritifcate file to load
   * @return X509Certificate of the specified file
   * @throws CertificateException
   * @throws IOException
   */
  public static X509Certificate loadCert(String certFile) throws CertificateException, IOException {
    File f = new File(certFile);
    if (f.exists()) {
      FileInputStream fis = null;
      CertificateFactory certFact = CertificateFactory.getInstance("X.509");
      try {
        fis = new FileInputStream(certFile);
        return (X509Certificate) certFact.generateCertificate(fis);
      }
      finally {
        fis.close();
      }
    }
    else {
      throw new IOException("No certificate found");
    }
  }

  /**
   * Utility method for showing the certificate details of the specified certificate. The first step is to call loadCert to get a
   * certificate object and then pass it to this method.
   * 
   * This will show the Version, Subject, Sig Algorithm, Validity, Serial and Issuer
   * 
   * @param cert
   *          X509Certificate to display details of
   */
  public static void displayCertificate(X509Certificate cert) {
    System.out.println("Version:" + cert.getVersion());
    System.out.println("Subject:" + cert.getSubjectDN().toString());
    System.out.println("Signature Algorithm:" + cert.getSigAlgName());
    System.out.println("Validity:" + cert.getNotBefore() + " to " + cert.getNotAfter());
    System.out.println("Serial:" + cert.getSerialNumber().toString(16).toUpperCase());
    System.out.println("Issuer:" + cert.getIssuerDN().toString());
  }

  /**
   * Main method to handle calls to the CA
   * 
   * @param args
   *          String array of arguments
   * @throws IOException
   * @throws CAEntryException
   * @throws CertificateException
   */
  public static void main(String[] args) throws IOException, CAEntryException, CertificateException {
    if (args.length == 0) {
      printUsage();
      System.exit(1);
    }

    if (args[0].equalsIgnoreCase("list")) {
      CA ca = null;
      if (args.length == 2) {
        ca = new CA(args[1]);
      }
      else if (args.length > 2) {
        printUsage();
      }
      else {
        ca = new CA();
      }
      if (ca != null) {
        System.out.println("CertificateDB");
        System.out.println("=============");
        System.out.println(ca.list());
        System.exit(0);
      }
      System.exit(1);
    }
    else if (args[0].equalsIgnoreCase("reinstate")) {
      CA ca = null;
      if (args.length == 3) {
        ca = new CA(args[2]);
      }
      else if (args.length > 3 || args.length < 2) {
        printUsage();
      }
      else if (args.length == 2) {
        ca = new CA();
      }
      if (ca != null) {
        
        X509Certificate cert = CA.loadCert(args[1]);
        System.out.println("Reinstating: " + cert.getSubjectDN().toString());
        try {
          ca.reinstate(cert);
          ca.save();
          System.out.println("Certificate Reinstated");
          System.out.println("A new CRL should be produced using genCRL script");
          System.exit(0);
        }
        catch (ModificationException e) {
          System.out.println("Error: " + e.getMessage());
        }
      }
      System.exit(1);

    }
    else if (args[0].equalsIgnoreCase("reinstateserial")) {
      CA ca = null;
      if (args.length == 3) {
        ca = new CA(args[2]);
      }
      else if (args.length > 3 || args.length < 2) {
        printUsage();
      }
      else if (args.length == 2) {
        ca = new CA();
      }
      if (ca != null) {
        try {
          System.out.println("Reinstating: " + args[1]);
          ca.reinstate(args[1]);
          ca.save();
          System.out.println("Certificate Reinstated");
          System.out.println("A new CRL should be produced using genCRL script");
          System.exit(0);
        }
        catch (ModificationException e) {
          System.out.println("Error: " + e.getMessage());
        }
      }
      System.exit(1);
    }
    else if (args[0].equalsIgnoreCase("view")) {
      if (args.length != 2) {
        printUsage();
      }
      else {
        X509Certificate cert = CA.loadCert(args[1]);
        CA.displayCertificate(cert);
        System.exit(0);
      }
      System.exit(1);
    }else{
      printUsage();
    }
    System.exit(1);
  }

  /**
   * Usage instructions for CA
   */
  private static void printUsage() {
    System.out.println("CA Usage");
    System.out.println("========");
    System.out.println("CA [options] [indexFile]");
    System.out.println("[options]");
    System.out.println("\tlist");
    System.out.println("\treinstate {certFile}");
    System.out.println("\t\t{certFile} is the path to the certificate to reinstate");
    System.out.println("\treinstateserial {serialno}");
    System.out.println("\t\t{serialno} is the serial number to reinstate");
    System.out.println("[indexFile] - path to index file, optional, if not specified uses index.txt");
  }

}
